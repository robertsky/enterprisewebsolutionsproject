$(document).ready(function () {
    $('#tagCloud a').tagcloud({
        size: {
            start: 14,
            end: 18,
            unit: 'pt'
        },
        color: {
            start: '#cde',
            end: '#f52'
        }
    });
    $('#tagCloudTable').dataTable({
    	"aaSorting": [[2, "asc"]]
    });
})